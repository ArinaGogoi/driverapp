import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {Http} from '@angular/http';
import { HttpClient} from '@angular/common/http';

import 'rxjs/add/operator/map';

import	{ DriverPage } from '../driver/driver'

import {AlertController} from 'ionic-angular';

import{ DriverAvailPage } from '../driver-avail/driver-avail';

import { AngularFireDatabase } from 'angularfire2/database';
import firebase from 'firebase';

declare var FCMPlugin;





@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
      //firestore = firebase.database().ref('/pushTokenDrivers');
     // 
      firestore = firebase.database().ref('/pushTokens');

  
  constructor(public navCtrl: NavController,public http: Http,public alertCtrl: AlertController, public afd: AngularFireDatabase) {
    
    //firebase.auth().signInWithEmailAndPassword('niloy@gmail.com', '123456')
    this.tokenSetup().then((token) => {
      this.storeToken(token);
    });
  }

  ionViewDidLoad(){
    //Here you define your application behaviour based on the notification data.
  FCMPlugin.onNotification(function(data){

//var obj = JSON.parse(data)

    if(data.wasTapped){
      //Notification was received on device tray and tapped by the user.
       //let obj = JSON.parse(data)
//alert(data);
      alert( JSON.stringify(data) );
      this.navCtrl.push(DriverAvailPage);
    }else{
      //Notification was received in foreground. Maybe the user needs to be notified.
      alert( JSON.stringify(data) );
      //let obj = JSON.parse(data)
      //alert(data);
      this.navCtrl.push(DriverAvailPage);
    }
  });
  
  //Note that this callback will be fired everytime a new token is generated, including the first time.
 /* FCMPlugin.onTokenRefresh(function(token){
    alert( token );
  });*/
  }


    tokenSetup() {
      var promise = new Promise((resolve, reject) =>{
        FCMPlugin.getToken(function(token){
          resolve(token);
      }, (err) =>{
        reject(err);
      });
    })
    
    return promise;
    
    }
    
    
    storeToken(t) {
      

      
      if(firebase.database().ref('/pushTokens/{pushTokens}/{pushTokens}') != t){
      this.firestore.push({
       //uid : firebase.auth().currentUser.uid, 
        devToken: t
      }).then(() => 
        alert('Token stored'),
      //})/*
       err =>
        alert('Token not stored')
      );
    }
     /* this.afd.list(this.firestoreAdmin).push({
        //uid : firebase.auth().currentUser.uid, 
        devToken: t
      }).then(() => 
        alert('Token stored'),
      //})/*
       err =>
        alert('Token not stored')
      );*/
    
    
      /*this.afd.list(this.firemsg).push({ 
        sendername: 'Admin', 
        message: 'Admin says Hello!!'
      }).then(() => 
        alert('Message Stored'),
      //})
      err => 
        alert('Message not stored')
      );*/
    }

 

    gopage(U,p){

      //let url="http://localhost:5050/api/test";
      let url="http://ec2-34-210-132-6.us-west-2.compute.amazonaws.com:3000/api/test";
      let  param = {Username:U,password:p };
      
      this.http.post(url,param).map(res=>res.json()).subscribe(data=> {
        console.log(data.res);
        if(data.res==='yes'){
          

          //this.navCtrl.push(DriverPage);
          //firebase.auth().signInWithEmailAndPassword('niloy@gmail.com', '123456');
          this.navCtrl.push(DriverAvailPage)

        }  
        else if(data.res==='invalid'){ 
            this.alertCtrl.create({
  
            title:"login failed",
            message:"invalid username or password"
        }).present();
        }
        else{
         // this.navCtrl.push(DriverAvailPage)
          this.navCtrl.push(DriverPage);
        }
      }
     
  );  
      }
}

