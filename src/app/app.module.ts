import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import {HttpModule} from '@angular/http';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase } from 'angularfire2/database';

import{HttpClientModule} from '@angular/common/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import	{ DriverPage} from '../pages/driver/driver';
import {SubmitPage} from '../pages/submit/submit';
import { DriverAvailPage } from '../pages/driver-avail/driver-avail';
import { QuickBookingPage } from '../pages/quick-booking/quick-booking';
import { NetworkEngineProvider } from '../providers/network-engine/network-engine';


var config = {
  apiKey: "AIzaSyDOYpywpULpfn_-NG2sbEQJoH6SB1A-c0Q",
  authDomain: "dev2devpush.firebaseapp.com",
  databaseURL: "https://dev2devpush.firebaseio.com",
  projectId: "dev2devpush",
  storageBucket: "",
  messagingSenderId: "46879361289"
};



@NgModule({
  declarations: [
    MyApp,
    DriverPage,
    SubmitPage,
    DriverAvailPage,
    QuickBookingPage,
    HomePage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(config)
    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    DriverPage,
    SubmitPage,
    DriverAvailPage,
    QuickBookingPage,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AngularFireDatabase,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    NetworkEngineProvider
  ]
})
export class AppModule {}
