import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
//import {Http,Headers} from '@angular/http';
import { HttpClient} from '@angular/common/http';
import { NetworkEngineProvider } from '../../providers/network-engine/network-engine';
import { AngularFireDatabase } from 'angularfire2/database';
import firebase from 'firebase';
//import { NetworkEngineProvider } from '../../providers/network-engine/network-engine';


/**
 * Generated class for the DriverAvailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-driver-avail',
  templateUrl: 'driver-avail.html',
})
export class DriverAvailPage {

firemsg = firebase.database().ref('/driverNotify');

//count = 0;
  
 // responseTxt: any;
  drivers = [];
 // newEl: string = "";


  constructor(public navCtrl: NavController, public navParams: NavParams, http: HttpClient, public network: NetworkEngineProvider, public afd: AngularFireDatabase) {
    this.showTable()
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DriverAvailPage');
  }

  

  showTable() {
    
    this.network.readTable().then(data => {
      //var drivers = []
     // var obj = [][];

      for(var j in data){
        //obj[j][name] =  data[j][name];
        this.drivers.push(data[j].Name)
      }
      //this.drivers.push(JSON.stringify(data[j]))

      console.log("Recived:" + JSON.stringify(data))

      /*for(var i in this.drivers)
      this.responseTxt = this.drivers[i];*/

      //var newlist = document.createElement("ul");
      //document.getElementById('driver').appendChild(newlist);

     /*for(var i in drivers){
        //document.write("<ion-list><ion-item><h2 (click)=notify()> " + drivers[i] + "</h2></ion-item><br>");
        //document.write("<ion-item>" + drivers[i] + "</ion-item></ion-list>");*/


    /*  var myvar= drivers[i];
      //var newItem=document.createElement("ul")
      var newh2 = document.createElement("li")
      newh2.setAttribute("id", "header2");
      var newtext=document.createTextNode(myvar);
      newlist.appendChild(newh2);
      //newItem.appendChild(newh2);
      newh2.appendChild(newtext);
      //document.getElementById('driver').appendChild(newlist);
      document.getElementById("header2").addEventListener("click", this.notify);
      
    }*/
      //this.count++
      // "" + JSON.stringify(data);
    })
  }

  notify(){
    //var string = "New Task Assigned"
    this.afd.list(this.firemsg).push({ 
      sendername: 'Admin', 
      message: 'New Task Assigned'
    }).then(() => 
      alert('Message Stored'),
    //})
    err => 
      alert('Message not stored')
    );
  }

  /*addElements() {
    for(var k in this.drivers)
    this.newEl = `<h2 (click)="notify()">{{drivers[${k}]}</h2>`
  } */


}
