import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the NetworkEngineProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class NetworkEngineProvider {

  constructor(public http: HttpClient) {
    console.log('Hello NetworkEngineProvider Provider');
  }

  
readTable() : Promise<any>
{
  let url="http://ec2-34-210-132-6.us-west-2.compute.amazonaws.com:3000/getposts";
  let request =this.http.get(url);
  return request.toPromise();

}

}
